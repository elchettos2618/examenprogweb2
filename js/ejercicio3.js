const valorImagen = document.getElementById('valorImagen');
const imagen = document.getElementById('imagen');

valorImagen.addEventListener('change', function() {
    if (valorImagen.files.length > 0) {
        const file = valorImagen.files[0]; 
        const objectURL = URL.createObjectURL(file); 

        imagen.src = objectURL;
    } else {
        imagen.src = '';
    }
});